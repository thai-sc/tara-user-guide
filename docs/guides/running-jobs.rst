==================
Running Jobs
==================

.. only:: html

    .. sidebar:: Page Contents

        .. contents:: 
            :local:


Job Accounting
===============

Slurm Job Scheduler
=====================

Tara uses the `Slurm Workload Manager <https://slurm.schedmd.com/>`_ as job scheduler. 
Slurm commands enable you to submit, manage, monitor, and control your jobs.

Slurm Partitions (Queues)
=========================

Submitting Batch Jobs
=======================

Launching Applications
=======================

Serial Application
------------------------------

MPI Application
------------------------------

To run more MPI processes than your allocation, use ``--overcommit`` flag when running. 

For example:

.. code:: bash

    [tara-frontend-1]$ srun -N 6 -n 12 -p standby --pty bash

    [tara-c-001]$ srun -n 40 --overcommit ./a.out

Notes on MPICH 
~~~~~~~~~~~~~~~

Running an MPICH program requires ``--mpi=pmi2`` flags for the program to run correctly. 
As of version 3.3, MPICH does not support PMIx.  

For example: 

.. code:: bash

    [tara-c-001]$ srun --mpi=pmi2 ./a.out


GPU Application on GPU Node
--------------------------------------

GPU Application on DGX-1
------------------------------------

Interactive Sessions
=======================

Monitoring Jobs and Queues
===========================

Other Job Management Commands
================================


