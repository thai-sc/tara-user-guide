===========================
File Storage and Transfer
===========================

.. only:: html

    .. sidebar:: Page Contents

        .. contents:: 
            :local:

Storage Options
================

File storage options on Tara systems include long-term storage (``$HOME``) 
and short-term storage (``$SCRATCH`` and ``/tmp``). Each option has different 
performance and intended uses. Files will be regularly purged from ``$SCRATCH``. 
See. :numref:`tara-storage-spec` and  for more details.

HOME Directory
----------------

SCRATCH Space
---------------

/tmp Directory
----------------

Environment Variables
=======================

During startup, corresponding account-level environment variables ``$HOME`` and ``$SCRATCH`` 
will be set as shown in :numref:`tara-storage-spec`

.. csv-table:: Environment Variables for File Systems
    :header: "Environment Variables", "Description"
    :name: file-env-var
    :widths: auto

    ``$HOME``,  "Path to home directory"
    ``$SCRATCH``, "Path to scratch filesystem"

Storage Quota / Limits
========================


Transferring Files
====================






