======================
Accessing the System
======================

.. only:: html

    .. sidebar:: Page Contents

        .. contents:: 
            :local:

Access to Tara requires ThaiSC account (not NSTDA account), which could be requested through this `form <http://test>`_.

Secure Shell (SSH)
===================

Secure shell (SSH) is the recommended method to connect to Tara. 
SSH also comes with file transfer utilities such as ``scp`` and ``sftp``. 

Linux and MacOS
-----------------

The ``ssh`` command is pre-installed on most Linux distributions. 
For MacOS, the command could be found in the Terminal application. 

Windows
--------

On Windows, an SSH client with SSH-2 protocol support, e.g. `PuTTY <https://www.putty.org/>`_ or 
`MobaXTerm <https://mobaxterm.mobatek.net/download.html>`_ is needed. 

Logging In
============

To submitting jobs on Tara system, login in to |tara-url| using the ``ssh`` command or 
the equivalent. On MacOS and Linux command, the command should look as follows

.. parsed-literal:: 
    :class: code

    $> ssh username@\ |tara-url|

When you log in to |tara-url|, you will be assigned one of the two frontend nodes: |tara-fe-url|
Users should normally log in through |tara-url|, but may specify one of the four nodes directly 
if they see poor performance.

.. note:: Do **NOT** use the frontend nodes for computationally intensive processes. These nodes are meant for compilation, 
          file editing, simple data analysis, and other tasks that use minimal compute resources. All computationally 
          demanding jobs should be submitted and run through the queuing system.

Logging In with X11 Support 
-----------------------------

Applications with graphical interface, e.g. MATLAB, will require X11 support.
To use X11, you will need to have a local X11 server running on your machine. 

    - **Linux**: X11 is pre-installed if your use Linux with graphical desktop. 
    - **MacOS**: `XQuartz <https://www.xquartz.org/>`_ is an open-source X Windows (X11) system for MacOS. 
    - **Windows**: `MobaXTerm <https://mobaxterm.mobatek.net/download.html>`_ comes with X11 support. 

To connect with X11 support (usually required for applications with graphical user 
interfaces, e.g. MATLAB), use the ``-X`` or ``-Y`` options: 

.. parsed-literal:: 
    :class: code

    $> ssh -X username@\ |tara-url|

.. |tara-url| replace:: **tara.nstda.or.th**
.. |tara-fe-url| replace:: **tara-frontend-[001-002].nstda.or.th**